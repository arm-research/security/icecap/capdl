{-# OPTIONS_GHC -fno-warn-unused-imports #-}
{-# OPTIONS_GHC -fno-warn-unused-matches #-}
{-# OPTIONS_GHC -fno-warn-unused-top-binds #-}
{-# OPTIONS_GHC -fno-warn-incomplete-patterns #-}
{-# OPTIONS_GHC -fno-warn-unused-local-binds #-}
{-# LANGUAGE DisambiguateRecordFields #-}
{-# LANGUAGE NamedFieldPuns #-}

module CapDL.DynDL.Transform
    -- ( toDynDL
    -- ) where
    where

import qualified CapDL.Model as C
import qualified Data.Map as M
import Data.Map (Map)
import CapDL.DynDL.Types
import Data.Maybe
import Data.Foldable
import Data.List
import Control.Exception (assert)
import Debug.Trace

toDynDL :: C.Model Word -> Model
toDynDL (C.Model _ objMap _ _ _) = Model model
  where
    (objects, names) = collectNames objMap
    toId = (M.!) names
    model = map ftop objects
    ftop ((name, isLocal), object) = TopObj name $ case isLocal of
        True -> Local (toObj object)
        False -> Extern $ case object of
            C.Notification {} -> ExternNotification
            C.Endpoint {} -> ExternEndpoint
            C.Frame { vmSizeBits } -> case vmSizeBits of
                12 -> ExternSmallPage
                21 -> ExternLargePage

    toObj object = case object of
        C.Untyped { C.maybeSizeBits = Just sizeBits }-> Obj_Untyped (ObjUntyped sizeBits)
        C.Endpoint -> Obj_Endpoint
        C.Notification -> Obj_Notification
        C.Frame { vmSizeBits, maybeFill } ->
            let fill = toFill maybeFill
            in case vmSizeBits of
                12 -> Obj_SmallPage (ObjSmallPage fill)
                21 -> Obj_LargePage (ObjLargePage fill)
        C.PT slots ->
            let slots' = M.mapKeys toInteger slots
                entries = M.map f slots'
                f cap = case toCap cap of
                    Cap_SmallPage cap' -> cap'
            in Obj_PT (ObjPT entries)
        C.PD slots ->
            let slots' = M.mapKeys toInteger slots
                entries = M.map f slots'
                f cap = case toCap cap of
                    Cap_PT cap' -> PDEntryPT cap'
                    Cap_LargePage cap' -> PDEntryLargePage cap'
            in Obj_PD (ObjPD entries)
        C.PUD slots ->
            let slots' = M.mapKeys toInteger slots
                entries = M.map f slots'
                f cap = case toCap cap of
                    Cap_PD cap' -> cap'
            in Obj_PUD (ObjPUD entries)
        C.PGD slots ->
            let slots' = M.mapKeys toInteger slots
                entries = M.map f slots'
                f cap = case toCap cap of
                    Cap_PUD cap' -> cap'
            in Obj_PGD (ObjPGD entries)
        C.CNode slots sizeBits ->
            let slots' = M.mapKeys toInteger slots
                entries = M.map toCap slots'
            in Obj_CNode (ObjCNode sizeBits entries)
        C.VCPU -> Obj_VCPU
        C.TCB
            { slots
            , faultEndpoint
            , extraInfo = Just extra_info
            , initArguments
            } ->
            let C.TCBExtraInfo
                    { ipcBufferAddr
                    , ip = Just ip
                    , sp = Just sp
                    , spsr = Just spsr
                    , prio = Just prio
                    , max_prio = Just max_prio
                    , affin = Just affinity
                    , resume
                    } = extra_info
                cspace = case toCap (slots M.! C.tcbCTableSlot) of
                    Cap_CNode cap -> cap
                vspace = case toCap (slots M.! C.tcbVTableSlot) of
                    Cap_PGD cap -> cap
                ipc_buffer = case toCap (slots M.! C.tcbIPCBufferSlot) of
                    Cap_SmallPage cap -> cap
                vcpu = (\(Cap_VCPU cap) -> cap) . toCap <$> (slots M.!? C.tcbBoundVCPUSlot)
                bound_notification = (\(Cap_Notification cap) -> cap) . toCap <$> (slots M.!? C.tcbBoundNotificationSlot)
            in Obj_TCB (ObjTCB
                { fault_ep = fromMaybe 0 faultEndpoint
                , cspace
                , vspace
                , ipc_buffer
                , ipc_buffer_addr = ipcBufferAddr
                , vcpu
                , bound_notification

                , affinity = fromIntegral affinity
                , prio = fromIntegral prio
                , max_prio = fromIntegral max_prio
                , resume = fromMaybe True resume

                , ip
                , sp
                , spsr
                , gprs = initArguments
                })
        x -> traceShow x undefined

    toRights :: C.CapRights -> Rights
    toRights = foldr f emptyRights
      where
        f right acc = case right of
            C.Read -> acc { right_read = True }
            C.Write -> acc { right_write = True }
            C.Grant -> acc { right_grant = True }
            C.GrantReply -> acc { right_grantReply = True }

    toCap cap = case cap of
        C.UntypedCap capObj -> Cap_Untyped (CapUntyped (toId capObj))
        C.EndpointCap capObj capBadge capRights -> Cap_Endpoint (CapEndpoint (toId capObj) capBadge (toRights capRights))
        C.NotificationCap capObj capBadge capRights -> Cap_Notification (CapNotification (toId capObj) capBadge (toRights capRights))
        C.CNodeCap capObj capGuard capGuardSize -> Cap_CNode (CapCNode (toId capObj) capGuard capGuardSize)
        C.TCBCap capObj -> Cap_TCB (CapTCB (toId capObj))
        C.VCPUCap capObj -> Cap_VCPU (CapVCPU (toId capObj))
        C.FrameCap { capObj, capRights, capCached } ->
            let Just (C.Frame { vmSizeBits }) = M.lookup capObj objMap
                id = toId capObj
                rights = toRights capRights
            in case vmSizeBits of
                12 -> Cap_SmallPage (CapSmallPage id rights capCached)
                21 -> Cap_LargePage (CapLargePage id rights capCached)
        C.PTCap capObj _ -> Cap_PT (CapPT (toId capObj))
        C.PDCap capObj _ -> Cap_PD (CapPD (toId capObj))
        C.PUDCap capObj _ -> Cap_PUD (CapPUD (toId capObj))
        C.PGDCap capObj _ -> Cap_PGD (CapPGD (toId capObj))

getName :: C.ObjID -> (String, Bool)
getName (name, Nothing) = case stripPrefix "extern_" name of
    Nothing -> (name, True)
    Just inner -> (inner, False)

collectNames :: C.ObjMap a -> ([((String, Bool), C.KernelObject a)], Map C.ObjID Integer)
collectNames map = (reverse objs, map')
  where
    (_, objs, map') = M.foldrWithKey f (0, [], M.empty) map
    f objId obj (i, objs, map') = assert (M.notMember objId map') $
        (i + 1, (getName objId, obj):objs, M.insert objId i map')

toFill :: Maybe [[String]] -> Fill
toFill = map f . concat . toList
  where
    f (dest_offset:dest_len:"CDL_FrameFill_FileData":file:file_offset:[]) = FillEntry
        (read dest_offset)
        (read dest_len)
        (tail (init file))
        (read file_offset)
