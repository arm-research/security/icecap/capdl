{-# OPTIONS_GHC -fno-warn-unused-imports #-}
{-# OPTIONS_GHC -fno-warn-unused-matches #-}
{-# OPTIONS_GHC -fno-warn-unused-top-binds #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

module CapDL.DynDL.Print
    ( printDynDL
    ) where

import CapDL.DynDL.Types
import Data.Aeson
import Data.ByteString.Lazy.Char8 as C
import Data.Text (Text)

printDynDL :: Model -> String
printDynDL = C.unpack . encode

instance ToJSON Model where
    toJSON (Model objs) = object [ "objects" .= toJSON objs ]

instance ToJSON TopObj where
    toJSON (TopObj name obj) = object
        [ "object" .= toJSON obj
        , "name" .= toJSON name
        ]

instance ToJSON AnyObj where
    toJSON (Local obj) = tagged "Local" obj
    toJSON (Extern obj) = tagged "Extern" $ case obj of
        ExternEndpoint -> "Endpoint" :: Text
        ExternNotification -> "Notification"
        ExternSmallPage -> "SmallPage"
        ExternLargePage -> "LargePage"

instance ToJSON Obj where
    toJSON obj = case obj of
        Obj_Untyped obj -> tagged "Untyped" obj
        Obj_Endpoint -> String "Endpoint"
        Obj_Notification -> String "Notification"
        Obj_CNode obj -> tagged "CNode" obj
        Obj_TCB obj -> tagged "TCB" obj
        Obj_VCPU -> String "VCPU"
        Obj_SmallPage obj -> tagged "SmallPage" obj
        Obj_LargePage obj -> tagged "LargePage" obj
        Obj_PT obj -> tagged "PT" obj
        Obj_PD obj -> tagged "PD" obj
        Obj_PUD obj -> tagged "PUD" obj
        Obj_PGD obj -> tagged "PGD" obj

instance ToJSON ObjUntyped where
    toJSON ObjUntyped{..} = object [ "size_bits" .= sizeBits ]

instance ToJSON ObjCNode where
    toJSON ObjCNode{..} = object
        [ "size_bits" .= sizeBits
        , "entries" .= entries
        ]

instance ToJSON Cap where
    toJSON cap' = case cap' of
        Cap_Untyped cap -> tagged "Untyped" cap
        Cap_Endpoint cap -> tagged "Endpoint" cap
        Cap_Notification cap -> tagged "Notification" cap
        Cap_CNode cap -> tagged "CNode" cap
        Cap_TCB cap -> tagged "TCB" cap
        Cap_VCPU cap -> tagged "VCPU" cap
        Cap_SmallPage cap -> tagged "SmallPage" cap
        Cap_LargePage cap -> tagged "LargePage" cap
        Cap_PT cap -> tagged "PT" cap
        Cap_PD cap -> tagged "PD" cap
        Cap_PUD cap -> tagged "PUD" cap
        Cap_PGD cap -> tagged "PGD" cap
