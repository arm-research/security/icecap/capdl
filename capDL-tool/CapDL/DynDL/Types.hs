{-# OPTIONS_GHC -fno-warn-unused-imports #-}
{-# OPTIONS_GHC -fno-warn-unused-matches #-}
{-# OPTIONS_GHC -fno-warn-unused-top-binds #-}
{-# LANGUAGE DuplicateRecordFields #-}
{-# LANGUAGE DeriveAnyClass #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE RecordWildCards #-}

module CapDL.DynDL.Types
    where

import qualified Data.Map as Map
import Data.Aeson
import Data.Text (Text)
import Data.Void (Void)
import GHC.Generics

type ObjId = Integer
type Table = Map.Map Integer

data Obj =
      Obj_Untyped ObjUntyped
    | Obj_Endpoint
    | Obj_Notification
    | Obj_CNode ObjCNode
    | Obj_TCB ObjTCB
    | Obj_VCPU
    | Obj_SmallPage ObjSmallPage
    | Obj_LargePage ObjLargePage
    | Obj_PT ObjPT
    | Obj_PD ObjPD
    | Obj_PUD ObjPUD
    | Obj_PGD ObjPGD
    deriving (Eq, Show)

data Cap =
      Cap_Untyped CapUntyped
    | Cap_Endpoint CapEndpoint
    | Cap_Notification CapNotification
    | Cap_CNode CapCNode
    | Cap_TCB CapTCB
    | Cap_VCPU CapVCPU
    | Cap_SmallPage CapSmallPage
    | Cap_LargePage CapLargePage
    | Cap_PT CapPT
    | Cap_PD CapPD
    | Cap_PUD CapPUD
    | Cap_PGD CapPGD
    deriving (Eq, Show)

type Badge = Word

data Rights = Rights
    { right_read :: Bool
    , right_write :: Bool
    , right_grant :: Bool
    , right_grantReply :: Bool
    } deriving (Eq, Show)

emptyRights :: Rights
emptyRights = Rights False False False False

type CPtr = Word
type Fill = [FillEntry]

data FillEntry = FillEntry
    { fillOffset :: Word
    , fillLength :: Word
    , fillFile :: String
    , fillFileOffset :: Word
    } deriving (Eq, Show)

data PDEntry = PDEntryPT CapPT | PDEntryLargePage CapLargePage
    deriving (Eq, Show)

data ObjUntyped = ObjUntyped
    { sizeBits :: Word
    } deriving (Eq, Show)

data ObjEndpoint = ObjEndpoint
    { obj :: ObjId
    , badge :: Badge
    , rights :: Rights
    } deriving (Eq, Show)

data ObjNotification = ObjNotification
    { obj :: ObjId
    , badge :: Badge
    , rights :: Rights
    } deriving (Eq, Show)

data ObjCNode = ObjCNode
    { sizeBits :: Word
    , entries :: Table Cap
    } deriving (Eq, Show)

data ObjTCB = ObjTCB
    { fault_ep :: CPtr
    , cspace :: CapCNode
    , vspace :: CapPGD
    , ipc_buffer :: CapSmallPage
    , ipc_buffer_addr :: Word
    , vcpu :: Maybe CapVCPU
    , bound_notification :: Maybe CapNotification

    , affinity :: Word
    , prio :: Word
    , max_prio :: Word
    , resume :: Bool

    , ip :: Word
    , sp :: Word
    , spsr :: Word
    , gprs :: [Word]
    } deriving (Eq, Show, Generic, ToJSON)

data ObjSmallPage = ObjSmallPage
    { fill :: Fill
    } deriving (Eq, Show, Generic, ToJSON)

data ObjLargePage = ObjLargePage
    { fill :: Fill
    } deriving (Eq, Show, Generic, ToJSON)

data ObjPT = ObjPT
    { entries :: Table CapSmallPage
    } deriving (Eq, Show, Generic, ToJSON)

data ObjPD = ObjPD
    { entries :: Table PDEntry
    } deriving (Eq, Show, Generic, ToJSON)

data ObjPUD = ObjPUD
    { entries :: Table CapPD
    } deriving (Eq, Show, Generic, ToJSON)

data ObjPGD = ObjPGD
    { entries :: Table CapPUD
    } deriving (Eq, Show, Generic, ToJSON)

data CapUntyped = CapUntyped
    { obj :: ObjId
    } deriving (Eq, Show, Generic, ToJSON)

data CapEndpoint = CapEndpoint
    { obj :: ObjId
    , badge :: Badge
    , rights :: Rights
    } deriving (Eq, Show, Generic, ToJSON)

data CapNotification = CapNotification
    { obj :: ObjId
    , badge :: Badge
    , rights :: Rights
    } deriving (Eq, Show, Generic, ToJSON)

data CapCNode = CapCNode
    { obj :: ObjId
    , guard :: Word
    , guard_size :: Word
    } deriving (Eq, Show, Generic, ToJSON)

data CapTCB = CapTCB
    { obj :: ObjId
    } deriving (Eq, Show, Generic, ToJSON)

data CapVCPU = CapVCPU
    { obj :: ObjId
    } deriving (Eq, Show, Generic, ToJSON)

data CapSmallPage = CapSmallPage
    { obj :: ObjId
    , rights :: Rights
    , cached :: Bool
    } deriving (Eq, Show, Generic, ToJSON)

data CapLargePage = CapLargePage
    { obj :: ObjId
    , rights :: Rights
    , cached :: Bool
    } deriving (Eq, Show, Generic, ToJSON)

data CapPT = CapPT
    { obj :: ObjId
    } deriving (Eq, Show, Generic, ToJSON)

data CapPD = CapPD
    { obj :: ObjId
    } deriving (Eq, Show, Generic, ToJSON)

data CapPUD = CapPUD
    { obj :: ObjId
    } deriving (Eq, Show, Generic, ToJSON)

data CapPGD = CapPGD
    { obj :: ObjId
    } deriving (Eq, Show, Generic, ToJSON)


data ExternObj =
      ExternEndpoint
    | ExternNotification
    | ExternSmallPage
    | ExternLargePage
    deriving (Eq, Show)

data Model = Model [TopObj]
    deriving (Eq, Show)

data AnyObj = Local Obj | Extern ExternObj
    deriving (Eq, Show)

data TopObj = TopObj
    { topName :: String
    , topObj :: AnyObj
    } deriving (Eq, Show)

--

tagged :: ToJSON a => Text -> a -> Value
tagged tag value = object [ tag .= toJSON value ]

instance ToJSON Rights where
    toJSON Rights{..} = object
        [ "read" .= right_read
        , "write" .= right_write
        , "grant" .= right_grant
        , "grant_reply" .= right_grantReply
        ]

instance ToJSON PDEntry where
    toJSON (PDEntryPT obj) = tagged "PT" obj
    toJSON (PDEntryLargePage obj) = tagged "LargePage" obj

instance ToJSON FillEntry where
    toJSON FillEntry{..} = object
        [ "offset" .= fillOffset
        , "length" .= fillLength
        , "file" .= fillFile
        , "file_offset" .= fillFileOffset
        -- HACK for convenient reuse of types
        , "content_digest" .= ([] :: [Void])
        ]
